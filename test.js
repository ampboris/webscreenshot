const {takeScreenShot, listObjects, takeScreenShots} = require('./handler');
process.env.S3_BUCKET_FOR_IMAGE = 's3-webshots';
process.env.API_VERSION = '2006-03-01';
process.env.ACCEESS_KEY_ID = 'xxxxxx';
process.env.SECRET_KEY = 'xxxxxx';
process.env.FOLDER = '2017_09_09';
process.env.SCREEN_IMAGE_RENDER_TIMEOUT = 4000;
process.env.SCREEN_IMAGE_WIDTH = 1024;
process.env.SCREEN_IMAGE_HEIGHT = 768;

takeScreenShots({body: '{"urls": ["http://www.google.com/", "https://www.smh.com.au/"]}'}, null, (x, y) => {
    console.log(y);
});

takeScreenShot({body: '{"url": "http://www.usopen.org/"}'}, null, (x, y) => {
   console.log(y);
});
// takeScreenShot({body: '{"url": "https://www.sinorbis.com/"}'}, null, (x, y) => {
//     console.log(y);
// });
//
// takeScreenShot({body: '{"url": "https://www.nytimes.com/"}'}, null, (x, y) => {
//     console.log(y);
// });
//
// takeScreenShot({body: '{"url": "https://www.smh.com.au/"}'}, null, (x, y) => {
//     console.log(y);
// });
listObjects(null, null, (x, y) => {
   console.log(y);
});