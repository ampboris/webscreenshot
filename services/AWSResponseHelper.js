'use strict';
class AWSResponseHelper {
    buildResponse (statusCode, message, awsCallback) {
        const response = {
            statusCode: statusCode,
            body: JSON.stringify(message)
        };
        awsCallback(null, response);
    }
}

module.exports = AWSResponseHelper;
