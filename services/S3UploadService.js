'use strict';
const fs = require('fs');
const AWS = require('aws-sdk');
const AWSResponseHelper = require('./AWSResponseHelper');
class ScreenShotService {
    constructor () {
        this.phantomTimeout = 40000; // 4s timeout
        this.awsResHelper = new AWSResponseHelper();
        this.s3Config = {
            apiVersion: process.env.API_VERSION,
            accessKeyId: process.env.ACCEESS_KEY_ID,
            secretAccessKey: process.env.SECRET_KEY,
            region: process.env.AWS_REGION
        };
        this.s3 = new AWS.S3(this.s3Config);
    }

    /**
     * 
     * @param {*} targetHash this is used to build filename
     * @param {*} targetFilename 
     * @param {*} targetBucket 
     * return promise
     */
    uploadToS3 (targetHash, targetFilename, targetBucket) {
        return new Promise((resolve, reject) => {
            try {
                let fileLocation = this.getSourceLocation (targetHash);
                fs.readFile(fileLocation, (err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        // upload the file
                        let folder = process.env.FOLDER;
                        this.s3.putObject({
                            ACL: 'public-read',
                            Key: folder.toString() + '/' + targetFilename,
                            Body: data,
                            Bucket: targetBucket,
                            ContentType: 'image/png',
                        }, (err) => {
                            if (err) {
                                console.warn(err);
                                reject(err);
                            } else {
                                resolve({
                                    msg:'upload is successful'
                                });
                            }
                        });
                    }
                });
            } catch (err) {
                eject(err);
            };
        });
    }

    /**
     * 
     * @param {*} targetHash 
     * @param {*} targetFilename 
     * @param {*} targetBucket 
     * 
     * return promise
     */
    uploadToS3Promise (targetHash, targetFilename, targetBucket) {
        let fileLocation = this.getSourceLocation (targetHash);
        return new Promise((resolve, reject) => {
            try {
                fs.readFile(fileLocation, (err, data) => {
                    if (err) {
                        console.log('fail : fs.readFile =>', err);
                        reject(err);
                    } else {
                        // upload the file
                        let folder = process.env.FOLDER;
                        this.s3.putObject({
                            ACL: 'public-read',
                            Key: folder.toString() + '/' + targetFilename,
                            Body: data,
                            Bucket: targetBucket,
                            ContentType: 'image/png',
                        }, (err) => {
                            if (err) {
                                console.log('fail : s3.putObject =>', err);
                                reject(err);
                            } else {
                                resolve({msg: 'done'});
                            }
                        });
                    }
                });
            } catch (exp) {
                console.log('fail : uploadToS3Promise function =>', err);
                reject(exp);
            }
        });
    }

    /**
     * return promise
     */
    listScreenShots () {
        let folder = process.env.FOLDER;
        let webHost = process.env.S3_PUBLIC_WEB_HOST || '';
        return new Promise((resolve, reject) => {
            try {
                this.s3.listObjects({
                    Bucket: process.env.S3_BUCKET_FOR_IMAGE,
                    Prefix: folder.toString(),
                }, (err, data) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        const list = [];
                        for (let content of data.Contents) {
                            list.push(webHost + content.Key);
                        }
                        // console.log('list of objects', list);
                        resolve({
                            images: list
                        });
                    }
                });
            } catch(err) {
                reject(err);
            }
        });
    }

    /**
     * 
     * @param {*} targetHash 
     * 
     * return file location
     * for test: `./test/${targetHash}.png`
     * for running local: `./${targetHash}.png`
     * for AWS lamda enviroment : `/tmp/${targetHash}.png`
     */
    getSourceLocation (targetHash) {
        // console.log('process.env.UNIT_TEST_MODE =>', process.env.UNIT_TEST_MODE);
        if(process.env.UNIT_TEST_MODE && process.env.UNIT_TEST_MODE === 'test') {
            return `./test/${targetHash}.png`;
        } else {
            return (process.env.AWS_REGION ? `/tmp/${targetHash}.png` : `./${targetHash}.png`);
 
        }
    }
}

module.exports = ScreenShotService;