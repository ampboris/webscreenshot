'use strict';
class PhantomCmdFactory {
    static buildPhantomCmd (targetUrl, targetHash) {
        let timeout = process.env.SCREEN_IMAGE_RENDER_TIMEOUT || 4000;
        let screenWidth = process.env.SCREEN_IMAGE_WIDTH || 1024;
        let screenHeight = process.env.SCREEN_IMAGE_HEIGHT || 768;
        let osName = process.env.AWS_REGION ? 'linux' : 'mac';
        console.log('process.env.UNIT_TEST_MODE:', process.env.UNIT_TEST_MODE);
        // // build the cmd for phantom to render the url
        let cmd = `./phantomjs/phantomjs_linux-x86_64 --ignore-ssl-errors=true ./phantomjs/screenshot.js ${targetUrl} /tmp/${targetHash}.png ${screenWidth} ${screenHeight} ${timeout}`; // eslint-disable-line max-len
        if (osName === 'mac') {
            cmd = `./phantomjs/phantomjs_osx --ignore-ssl-errors=true ./phantomjs/screenshot.js ${targetUrl} ./${targetHash}.png ${screenWidth} ${screenHeight} ${timeout}`;
        }
        if(process.env.UNIT_TEST_MODE && process.env.UNIT_TEST_MODE === 'test') {
            cmd = `../phantomjs/phantomjs_osx --ignore-ssl-errors=true ../phantomjs/screenshot.js ${targetUrl} ${targetHash}.png ${screenWidth} ${screenHeight} ${timeout}`;
        }
        console.log(cmd);
        return cmd;
    }
}

module.exports = PhantomCmdFactory;