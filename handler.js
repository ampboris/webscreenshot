const exec = require('child_process').exec;
const crypto = require('crypto');
const fs = require('fs');
const validUrl = require('valid-url');
const PhantomCmdFactory = require('./services/PhantomCmdFactory');
const S3UploadService = require('./services/S3UploadService');
const AWSResponseHelper = require('./services/AWSResponseHelper');
// overall constants
const DEFAULT_S3_STORAGE_BUCKET = 'unknown';

module.exports.takeScreenShot = (event, context, callback) => {
    let awsResponse = new AWSResponseHelper();
    try {
        const data = JSON.parse(event.body);
        const targetUrl = data.url;
        console.log(targetUrl);

        if (!validUrl.isUri(targetUrl)) {
            awsResponse.buildResponse(422, {err: 'not a valid url'}, callback);
            return;
        }

        console.log('process.env.S3_BUCKET_FOR_IMAGE:', process.env.S3_BUCKET_FOR_IMAGE);

        const targetBucket = process.env.S3_BUCKET_FOR_IMAGE || DEFAULT_S3_STORAGE_BUCKET;
        const targetHash = crypto.createHash('md5').update(targetUrl).digest('hex');
        const targetFilename = `${targetHash}.png`;

        const cmd = PhantomCmdFactory.buildPhantomCmd(targetUrl, targetHash);

        exec(cmd, { maxBuffer : 1024 * 768 * 5 },  (error, stdout, stderr) => {
            if (error) {
                // the command failed (non-zero), fail the entire call
                console.warn(`exec error: ${error}`, stdout, stderr);
                awsResponse.buildResponse(400, {err:error}, callback);
            } else {
                // snapshotting succeeded, let's upload to S3
                // read the file into buffer (perhaps make this async?)
                const uploadService = new S3UploadService();
                uploadService.uploadToS3(targetHash, targetFilename, targetBucket).then((resp) => {
                    awsResponse.buildResponse(200, {'msg' : 'done'}, callback);
                }).catch((err) => {
                    awsResponse.buildResponse(400, { err: err }, callback);
                });
            }
        });
    } catch (exp) {
        console.log('system error =>', exp);
        awsResponse.buildResponse(400, { err: exp, errType: 'generic', ref: 'check request' }, callback);
    }

};

module.exports.listObjects = (event, context, callback) => {
    let awsResponse = new AWSResponseHelper();
    try {
        const uploadService = new S3UploadService();
        uploadService.listScreenShots().then((resp) => {
            awsResponse.buildResponse(200, resp, callback);
        }).catch((err) => {
            awsResponse.buildResponse(400, { err: err }, callback);
        });
    } catch (exp) {
        console.log('system error =>', exp);
        awsResponse.buildResponse(400, { err: exp, errType: 'generic', ref: 'check request' }, callback);
    }
};

module.exports.takeScreenShots = (event, context, callback) => {
    let awsResponse = new AWSResponseHelper();
    try {
        const data = JSON.parse(event.body);
        const requestUrls = data.urls;

        if(!requestUrls || requestUrls.length < 1) {
            awsResponse.buildResponse(400, {'msg': 'no url to be processed.'}, callback);
            return 0;
        }
        //TODO: this should in configuration, but for test, put here
        if(requestUrls.length > 5) {
            awsResponse.buildResponse(400, {'msg': '# of url is limited to 5 for testing'}, callback);
            return 0;
        }

        //TODO: Do we need to check whether the urls are unique?

        let jobs = [];

        for(let targetUrl of requestUrls) {
            console.log(targetUrl);
            if (!validUrl.isUri(targetUrl)) {
                awsResponse.buildResponse(422, {err: 'not a valid url'}, callback);
                return;
            }
            let task = processUrlScreenShot(targetUrl);
            jobs.push(task);
        }

        Promise.all(jobs).then((responseList) => {
            //TODO: Could show each status if need.
            // for(let resp in responseList) {
            //     console.log(resp);
            // }
            awsResponse.buildResponse(200, {'msg' : 'done'}, callback);
        }).catch((errs) => {
            //TODO: Could show each status if need.
            awsResponse.buildResponse(400,
                JSON.stringify({msg: errs}), callback);
            return 0;
        });
    } catch (exp) {
        console.log('system error =>', exp);
        awsResponse.buildResponse(400, { err: exp.toString(), errType: 'generic', ref: 'check request' }, callback);
    }

};

function processUrlScreenShot (targetUrl) {
    return new Promise((resolve, reject) => {
        try {
            console.log('process.env.S3_BUCKET_FOR_IMAGE:', process.env.S3_BUCKET_FOR_IMAGE);

            const targetBucket = process.env.S3_BUCKET_FOR_IMAGE || DEFAULT_S3_STORAGE_BUCKET;
            const targetHash = crypto.createHash('md5').update(targetUrl).digest('hex');
            const targetFilename = `${targetHash}.png`;

            const cmd = PhantomCmdFactory.buildPhantomCmd(targetUrl, targetHash);

            exec(cmd, { maxBuffer : 1024 * 768 * 5 },  (error, stdout, stderr) => {
                if (error) {
                    // the command failed (non-zero), fail the entire call
                    console.warn(`exec error: ${error}`, stdout, stderr);
                    reject(error);
                } else {
                    // snapshotting succeeded, let's upload to S3
                    // read the file into buffer (perhaps make this async?)
                    const uploadService = new S3UploadService();
                    uploadService.uploadToS3Promise( targetHash, targetFilename, targetBucket).then((resp) => {
                        resolve(resp);
                    }).catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                }
            });
        } catch (exp) {
            console.log('system error =>', exp);
            reject({ err: exp});
        }
    });
};