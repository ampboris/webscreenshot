# webscreenshot
webscreenshot Service

This will setup a webscreenshot api which will take a screenshot from a given url, and push it into an S3 bucket. This is all done with Lambda calls. 

The screenshotting is done with PhantomJS (which is precompiled in this project).

Installation 
====================
# Pre-requisites

1. Node.js v6.5.0 or later.
2. Serverless CLI v1.9.0 or later. You can run npm install -g serverless to install it.
3. An AWS account. If you don't already have one, you can sign up for a free trial that includes 1 million free Lambda requests per month.
4. Set-up your Provider Credentials. (https://serverless.com/framework/docs/providers/aws/guide/credentials/)
5. AWS Command Line Interface (CLI)
(https://aws.amazon.com/cli/)

# Setup
Just install all requirements with npm:

```bash
npm install
```

# Installation
This project uses Serverless for setting up the service. 
Check the `serverless.yml` for the bucket name etc configuration, and change them to whatever your enviroment seetings. You can then deploy the stack with:

```bash
sls deploy -s dev

api key setup

```

# Usage

## Create screenshots from a list of urls
If you post a list of urls to the /webshots/ endpoint, it will create screenshots for you, in the example above:

```bash
  POST - https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshots

curl -X POST \
  https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshots \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 91b34e6c-c7be-97cb-9297-923af98d0782' \
  -H 'x-api-key: 91b34e6c-c7be-97cb-9297-923af98d0782' \
  -d '{
	"urls": [
		"https://www.amp.com.au/",
		"http://www.usopen.org/",
		"https://serverless.com/",
		"https://www.smh.com.au/",
		"http://www.news.com.au/"
	]
}'

```

## Create screenshot from an url
If you post a url to the /webshot/ endpoint, it will create a screenshot for you, in the example above:

```bash
  POST - https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshot

curl -X POST \
  https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshot \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 57c72f20-da64-def7-c676-812cc7d847a0' \
  -H 'x-api-key: 57c72f20-da64-def7-c676-812cc7d847a0' \
  -d '{
	"url": "https://www.nytimes.com/"
}'
  
```

## List available screenshot sizes
After creating a screenshot, you can see all the available screenshots in the S3 bucket with a GET:
```bash
	GET - https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshot
	curl -X GET \
	  https://2p8nwvrrob.execute-api.ap-southeast-2.amazonaws.com/dev/webshot \
	  -H 'cache-control: no-cache' \
	  -H 'postman-token: 28f8ae37-54f6-b3af-2927-6309332c02df' \
	  -H 'x-api-key: 28f8ae37-54f6-b3af-2927-6309332c02df'
```
# AWS throttle!!!!!

Do not forget about these tasks to avoid unexpected AWS bill!
* API key protection
* Usage plan (throttle)

# Caveats
* This service uses the awesome PhantomJS service to capture the screenshots, which is compiled on the Lambda service. There probably will be issues with fonts of some sorts.
* The default timeout for PhantomJS is set to 3 seconds, if the page takes longer to load, this will result in b0rked screenshots (ofcourse). You can change the timeout in handler.js (a PR with custom timeout is greatly appreciated)
* setup the Lambda function to allow for more memory usage.

# Permission and user setup

Create a user for access s3 bucket via sdk.
Create permission to the buckect, user and folder
```
{
    "Version": "2012-10-17",
    "Id": "Policy1505131614946",
    "Statement": [
        {
            "Sid": "Stmt1505131610895",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::590839738768:user/XXXXXXXX"
            },
            "Action": "s3:*",
            "Resource": "arn:aws:s3:::s3-webshots/2017_09_09/*"
        }
    ]
}
```
# TDD & BDD

Using Mocha and Chai for test
Preferably using Should and Expect for test!
npm install chai --save-dev
npm install mocha --save-dev