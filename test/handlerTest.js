var assert = require('chai').assert;
var expect = require('chai').expect;
var should = require('chai').should();
var request = require("request");
const {takeScreenShot, listObjects, takeScreenShots} = require('../handler');

describe('Test - handler.js', function() {
  this.timeout(15000);

  process.env.S3_BUCKET_FOR_IMAGE = 's3-webshots';
  process.env.API_VERSION = '2006-03-01';
  process.env.ACCEESS_KEY_ID = 'xxxxxx';
  process.env.SECRET_KEY = 'xxxxxx';
  process.env.FOLDER = '2017_09_09';
  process.env.SCREEN_IMAGE_RENDER_TIMEOUT = 4000;
  process.env.SCREEN_IMAGE_WIDTH = 1024;
  process.env.SCREEN_IMAGE_HEIGHT = 768;

  let webShotEndPoint = 'FILL_IN_THIS';
  let webShotsEndPoint = 'FILL_IN_THIS';
  let apiKey = 'FILL_IN_THIS';
  before(() => {
  });

  it('should take screen shots for a list of urls and upload them to S3 bucket', function(done) {
    this.timeout(40000);
    takeScreenShots({body: '{"urls": ["http://www.google.com/", "https://www.smh.com.au/"]}'}, null, (x, y) => {
      should.not.exist(x);
      should.exist(y);
      y.should.be.an('object');

      let resp = y;
      resp.should.be.not.null;
      should.exist(resp.statusCode);
      resp.statusCode.should.be.a('number');
      resp.statusCode.should.be.equal(200);
      should.exist(resp.body);
      resp.body.should.be.a('string');
        
      done();
    });
  }); 

  it('should take a screenshot for an url and upload to S3 bucket', function(done) {
    this.timeout(35000);
    takeScreenShot({body: '{"url": "http://www.usopen.org/"}'}, null, (x, y) => {
        should.not.exist(x);
        should.exist(y);
        y.should.be.an('object');

        let resp = y;
        resp.should.be.not.null;
        should.exist(resp.statusCode);
        resp.statusCode.should.be.a('number');
        resp.statusCode.should.be.equal(200);
        should.exist(resp.body);
        resp.body.should.be.a('string');

        //expect
        expect(x).to.be.null;
        expect(y).to.be.exist;
        expect(y).to.be.an('object');
        expect(resp).to.be.not.null;
        expect(resp.statusCode).to.be.a('number');
        expect(resp.statusCode).to.be.equal(200);
        expect(resp).to.be.have.property('body');
        done();
     });
  }); 

  it('should list all images in S3 bucket', function(done) {
    listObjects(null, null, (x, y) => {
        should.not.exist(x);
        should.exist(y);
        y.should.be.an('object');

        let resp = y;
        resp.should.be.not.null;
        should.exist(resp.statusCode);
        resp.statusCode.should.be.a('number');
        resp.statusCode.should.be.equal(200);
        should.exist(resp.body);
        resp.body.should.be.an('string');

        let list = JSON.parse(resp.body);
        should.exist(list.images);
        list.images.should.be.an('array').that.is.not.empty;
        list.images.length.should.be.gte(0);
        list.images.should.be.include('2017_09_09/test_2.png');
        done();
     });
  }); 


  // it('list images from webshot service works', function(done) {
  //   var options = { 
  //     method: 'GET',
  //     url: webShotEndPoint,
  //     headers: { 
  //      'x-api-key': apiKey 
  //     } 
  //   };
   
  //   request(options, (error, response, body) => {
  //     expect(response.statusCode).to.equal(200);
  //     let result = JSON.parse(body);
  //     expect(result).to.have.property('images');
  //     expect(result.images.length > 0).to.be.true;
  //     done();
  //   });
  // });
  // it('generate single web image works', function(done) {
  //   this.timeout(35000);
  //   var options = {
  //     method: 'POST',
  //     url: webShotEndPoint,
  //     headers: {
  //      'x-api-key': apiKey,
  //      'content-type': 'application/json' 
  //     },
  //     body: {
  //       url: 'https://www.dramacity.se/' 
  //     },
  //     json: true 
  //   };
   
  //   request(options, (error, response, body) => {
  //     expect(response.statusCode).to.equal(200);
  //     // console.log(body);
  //     // let result = JSON.parse(body);
  //     let result = body;
  //     expect(result).to.have.property('msg');
  //     expect(result.msg).to.be.equal('done');
  //     done();
  //   });
  // });
  // it('generate single web image works', function(done) {
  //   this.timeout(45000);
  //   var options = {
  //     method: 'POST',
  //     url: webShotsEndPoint,
  //     headers: {
  //      'x-api-key': apiKey,
  //      'content-type': 'application/json' 
  //     },
  //     body: { 
  //       urls: [ 
  //         'https://www.amp.com.au/',
  //         'http://www.usopen.org/',
  //         'https://serverless.com/',
  //         'https://www.smh.com.au/',
  //         'http://www.news.com.au/' 
  //       ] 
  //     },
  //     json: true 
  //   };
   
  //   request(options, (error, response, body) => {
  //     expect(response.statusCode).to.equal(200);
  //     // console.log(body);
  //     // let result = JSON.parse(body);
  //     let result = body;
  //     expect(result).to.have.property('msg');
  //     expect(result.msg).to.be.equal('done');
  //     done();
  //   });
  // });
});