var assert = require('chai').assert;
var expect = require('chai').expect;
var should = require('chai').should();
const S3UploadService = require('../../services/S3UploadService');

describe('Test - S3UploadService.js', function() {
  let service = new S3UploadService();
  let targetHash = 'test_2';
  let targetFilename = `${targetHash}.png`;
  let targetBucket = process.env.S3_BUCKET_FOR_IMAGE;

  this.timeout(15000);

  before(() => {
    // Make sure this is running here only, otherwise this settings may have impact to other test!
    process.env.AWS_REGION = 'ap-southeast-2';
    process.env.UNIT_TEST_MODE = 'test';
  })

  // it('test getSourceLocation : non-aws local source path', function() {
  //   var x = new S3UploadService().getSourceLocation('xxxxx');
  //   assert.equal('./xxxxx.png', x);
  // });
  it('should upload test image To S3 bucket', function(done) {
    service.uploadToS3(targetHash, targetFilename, targetBucket).then((resp) => {
      expect(resp).to.have.property('msg');
      expect(resp.msg).to.equal('upload is successful');
      done();
    }).catch((err) => {
      done(err);
    });
  }); 
  it('should list all images in S3 bucket', function(done) {
    service.listScreenShots().then((resp) => {
      expect(resp).to.not.be.null;
      expect(resp).to.have.property('images');
      expect(resp.images.length > 0).to.be.true;
      expect(resp.images).to.include('2017_09_09/test_2.png');
      done();
    }).catch((err) => {
      done(err);
    });
  });    
  it('should upload test image to S3 with Promise', function(done) {
    service.uploadToS3Promise(targetHash, targetFilename, targetBucket).then((resp) => {
      expect(resp).to.have.property('msg');
      expect(resp.msg).to.equal('done');
      done();
    }).catch((err) => {
      done(err);
    });
  });
  it('should return a test path', function() {
      x = new S3UploadService().getSourceLocation('xxxxx');
      should.exist(x);
      x.should.be.a('string');
      x.should.be.equal('./test/xxxxx.png');
  });  
});