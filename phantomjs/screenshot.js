var page = require('webpage').create();
var system = require('system');

console.log("input parameters: ", system.args);

// web url
var url = system.args[1];
// output location
var output = system.args[2];
// image and screen width
var width = system.args[3];
// image and screen height
var height = system.args[4];
// timeout for capturing process
var timeout = system.args[5];

//view port size
page.viewportSize = {
    width: parseInt(width),
    height: parseInt(height)
};
// Only capture size
page.clipRect = {
    top: 0,
    left: 0,
    width: parseInt(width) || 0,
    height: parseInt(height) || 0
};

page.onResourceReceived = function(response) {
    valid = [200, 201, 301, 302];
    if(response.id == 1 && valid.indexOf(response.status) == -1 ){
        console.log('Received a non-200 OK response, got: ', response.status);
        phantom.exit(1);
    }
};
/**
 * Open and render page.
 */
page.open(url, function (status) {
    if (status !== 'success') {
        console.log('Unable to load the url!');
        phantom.exit(1);
    }

    window.setTimeout(function () {
        page.evaluate(function () {
            if (!document.body.style.background) {
                document.body.style.backgroundColor = 'white'
            }
        });
        page.render(output);
        phantom.exit();
    }, timeout);
});
